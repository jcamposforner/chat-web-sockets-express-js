const express = require('express');
const path = require('path');
const app =express();

const SocketIo = require('socket.io');


app.set('port', process.env.PORT || 3000);


app.use(express.static(path.join(__dirname, 'public')));


const server = app.listen(app.get('port'),() => {
    console.log(`Server on port`,app.get('port'));
});


const io = SocketIo(server);

io.on('connection', (socket) => {
    console.log('new connection',socket.id)

    socket.on('chat:message', (data) => {
       console.log(data);
       io.sockets.emit('chat:message', data);
    });

    socket.on('chat:typing', data => {

        socket.broadcast.emit('chat:typing', data);
    })
});


console.log('a')