const socket = io();


//DOM

let message = document.getElementById('message');
let username = document.getElementById('username');
let btn = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');
let sound = new Audio('a.mp3');


document.addEventListener("DOMContentLoaded", function () {
    if(!Notification){
        alert('Navegador no soportado');

        return;
    }

    if(Notification.permission !== "granted"){
        Notification.requestPermission();
    }
});

function notificar(){

}

btn.addEventListener('click', function () {
   console.log('click');
   socket.emit('chat:message', {
       message: message.value,
       username: username.value
   });
});

message.addEventListener('keypress', function () {
   socket.emit('chat:typing', username.value);
});


socket.on('chat:message', function (data) {
    actions.innerHTML = '';
    if(data.username != username.value){
        sound.play();
        if(Notification.permission !== "granted"){
            Notification.requestPermission();
        }else{
            var notificacion = new Notification(data.username,{
                icon: "http://mejorcodigo.net/res/img/logo.png",
                body: data.message,
            });
        }

        notificacion.onclick = function () {
            window.open("http://google.es");
        }
    }

   output.innerHTML += '<p><strong>'+data.username+'</strong>:'+data.message+'</p>'
});

socket.on('chat:typing', function (data) {
    actions.innerHTML = '<p>'+data+' esta escribiendo</p>'
});

